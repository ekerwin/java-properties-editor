A properties file in Java has a particular syntax: https://docs.oracle.com/cd/E23095_01/Platform.93/ATGProgGuide/html/s0204propertiesfileformat01.html

We want to provide a convenient way to programmatically edit these files without loss of formatting, comments, or ordering. Blank lines, comments, etc should all be preserved, as much as possible.

Support for multiline values is not currently supported.

Suggested use:
```java
PropertiesFile propertiesFile = new PropertiesFile(File file);

propertiesFile.updateValue("favorite.pony", "Pinky Pie");
propertiesFile.updateValue("kindest.pony", "Fluttershy");
propertiesFile.updateValue("generous.pony", "Rarity");
...

propertiesFile.store();
```

You can also use `java.nio.file.Path` and/or an explicit `KeyValue` (or a `List` of them):
```java
List<KeyValue> keysToUpdate = List.of(
    new KeyValue("silliest.dragon", "Spike"),
    new KeyValue("big.jerk.face", "Discord"),
    new KeyValue("leader.of.equestria", "Princess Celestia")
);

PropertiesFile propertiesFile = new PropertiesFile(Path path);

propertiesFile.updateValues(keysToUpdate);

propertiesFile.store();
```

package com.kingofnerds.propertieseditor.exception;

import java.io.IOException;

public class PropertyFileNotUseableException extends EditorException {
    public PropertyFileNotUseableException(String path, IOException e) {
        super(String.format("The property file '%s' could not be used.", path), e);
    }

}

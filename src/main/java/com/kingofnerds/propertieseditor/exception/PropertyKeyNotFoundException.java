package com.kingofnerds.propertieseditor.exception;

public class PropertyKeyNotFoundException extends EditorException {
    public PropertyKeyNotFoundException(String propertyKey) {
        super(String.format("The key '%s' was not found.", propertyKey));
    }

}

package com.kingofnerds.propertieseditor.exception;

public class NotKeyValueException extends EditorException {
    public NotKeyValueException(String content) {
        super(String.format("The content '%s' is not a valid key/value pair.", content));
    }

}

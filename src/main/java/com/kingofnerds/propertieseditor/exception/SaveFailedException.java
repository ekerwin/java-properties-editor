package com.kingofnerds.propertieseditor.exception;

import java.io.IOException;

public class SaveFailedException extends EditorException {
    public SaveFailedException(String path, IOException e) {
        super(String.format("The file at the path '%s' could not be saved. Maybe a permissions issue?"), e);
    }

}

package com.kingofnerds.propertieseditor;

import com.kingofnerds.propertieseditor.exception.PropertyFileNotUseableException;
import com.kingofnerds.propertieseditor.exception.SaveFailedException;
import com.kingofnerds.propertieseditor.model.KeyValue;
import com.kingofnerds.propertieseditor.model.PropertyLines;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class PropertiesFile {
    private final Path propertiesFilePath;
    private final PropertyLines propertyLines;

    public PropertiesFile(File propertiesFile) {
        this(propertiesFile.toPath());
    }

    public PropertiesFile(Path propertiesFilePath) {
        this.propertiesFilePath = propertiesFilePath;

        try {
            List<String> lines = Files.readAllLines(propertiesFilePath, StandardCharsets.ISO_8859_1);
            propertyLines = new PropertyLines(lines);
        } catch (IOException e) {
            throw new PropertyFileNotUseableException(propertiesFilePath.toString(), e);
        }
    }

    public void updateValue(String key, String updatedValue) {
        updateValue(new KeyValue(key, updatedValue));
    }

    public void updateValues(List<KeyValue> updatedValues) {
        updatedValues.forEach(this::updateValue);
    }

    public void updateValue(KeyValue updatedValue) {
        propertyLines.updateValue(updatedValue);
    }

    public void store() {
        List<String> lines = propertyLines.getLines();
        try {
            Files.write(propertiesFilePath, lines, StandardCharsets.ISO_8859_1);
        } catch (IOException e) {
            throw new SaveFailedException(propertiesFilePath.toString(), e);
        }
    }

}

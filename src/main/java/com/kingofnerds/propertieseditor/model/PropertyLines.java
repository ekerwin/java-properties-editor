package com.kingofnerds.propertieseditor.model;

import com.kingofnerds.propertieseditor.exception.PropertyKeyNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PropertyLines {
    private final List<PropertyLine> propertyLines = new ArrayList<>();
    private final Map<String, PropertyLine> keysToLines = new HashMap<>();
    private final Map<String, Integer> keysToLineNumbers = new HashMap<>();

    public PropertyLines(List<String> lines) {
        for (int lineNumber = 0; lineNumber < lines.size(); lineNumber++) {
            PropertyLine propertyLine = new PropertyLine(lineNumber, lines.get(lineNumber));
            propertyLines.add(propertyLine);
            if (propertyLine.isKeyValue()) {
                keysToLines.put(propertyLine.getKeyValue().getKey(), propertyLine);
                keysToLineNumbers.put(propertyLine.getKeyValue().getKey(), lineNumber);
            }
        }
    }

    public List<String> getLines() {
        return propertyLines
                .stream()
                .map(PropertyLine::getContent)
                .collect(Collectors.toList());
    }

    public void updateValue(KeyValue updatedValue) {
        String key = updatedValue.getKey();
        if (!keysToLines.containsKey(key)) {
            throw new PropertyKeyNotFoundException(key);
        }

        int lineNumber = keysToLineNumbers.get(updatedValue.getKey());
        String oldContent = keysToLines.get(key).getContent();
        int valueIndex = oldContent.indexOf(key) + key.length() + 1;
        String newContent = oldContent.substring(0, valueIndex) + updatedValue.getValue();
        PropertyLine updatedLine = new PropertyLine(lineNumber, newContent);

        propertyLines.set(lineNumber, updatedLine);
    }

}

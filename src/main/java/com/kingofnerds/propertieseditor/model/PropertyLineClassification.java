package com.kingofnerds.propertieseditor.model;

public enum PropertyLineClassification {
    BLANK,
    COMMENT,
    STANDARD,
    NONSENSE;

}

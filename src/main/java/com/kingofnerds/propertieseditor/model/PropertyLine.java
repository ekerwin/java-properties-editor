package com.kingofnerds.propertieseditor.model;

import java.io.IOException;
import java.io.StringReader;
import java.util.EnumSet;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;

import com.kingofnerds.propertieseditor.exception.EditorException;
import com.kingofnerds.propertieseditor.exception.NotKeyValueException;

public class PropertyLine {
    public static final Set<Character> COMMENT_CHARS = Set.of('#', '!');

    private static final Properties PROPERTIES_UTIL = new Properties();

    private final int lineNumber;
    private final String content;
    private final Optional<String> key;
    private final Optional<String> value;
    private final EnumSet<PropertyLineClassification> lineFeatures;

    public PropertyLine(int lineNumber, String lineContent) {
        this.lineNumber = lineNumber;
        this.content = lineContent == null ? "" : lineContent;

        String trimmed = content.trim();
        if (trimmed.isBlank()) {
            lineFeatures = EnumSet.of(PropertyLineClassification.BLANK);
            key = Optional.empty();
            value = Optional.empty();
        } else if (COMMENT_CHARS.contains(trimmed.charAt(0))) {
            lineFeatures = EnumSet.of(PropertyLineClassification.COMMENT);
            key = Optional.empty();
            value = Optional.empty();
        } else {
            StringReader reader = new StringReader(content);
            try {
                PROPERTIES_UTIL.load(reader);
                Set<String> keys = PROPERTIES_UTIL.stringPropertyNames();
                if (keys.size() == 1) {
                    lineFeatures = EnumSet.of(PropertyLineClassification.STANDARD);
                    key = Optional.of(keys.iterator().next());
                    value = Optional.ofNullable(PROPERTIES_UTIL.getProperty(key.get()));
                } else {
                    lineFeatures = EnumSet.of(PropertyLineClassification.NONSENSE);
                    key = Optional.empty();
                    value = Optional.empty();
                }
            } catch (IOException e) {
                throw new EditorException("An unexpected exception while processing the property file.", e);
            }
            PROPERTIES_UTIL.clear();
        }
    }

    public Optional<String> getKey() {
        return key;
    }

    public Optional<String> getValue() {
        return value;
    }

    public KeyValue getKeyValue() throws NotKeyValueException {
        if (!isKeyValue()) {
            throw new NotKeyValueException(content);
        }

        return new KeyValue(key.get(), value.get());
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public String getContent() {
        return content;
    }

    public boolean isBlank() {
        return lineFeatures.contains(PropertyLineClassification.BLANK);
    }

    public boolean isComment() {
        return lineFeatures.contains(PropertyLineClassification.COMMENT);
    }

    public boolean isKeyValue() {
        return lineFeatures.contains(PropertyLineClassification.STANDARD);
    }

    public boolean isNonsense() {
        return lineFeatures.contains(PropertyLineClassification.NONSENSE);
    }

}

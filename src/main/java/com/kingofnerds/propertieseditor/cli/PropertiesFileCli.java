package com.kingofnerds.propertieseditor.cli;

import com.kingofnerds.propertieseditor.PropertiesFile;
import com.kingofnerds.propertieseditor.model.KeyValue;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

public class PropertiesFileCli {
    public static final String WRONG_ARGS = "Wrong number of arguments.";
    public static final String NOT_A_FILE = "Provided path is not an existing file.";

    public static void main(String[] args) {
        PropertiesFileCli propertiesFileCli = new PropertiesFileCli(args, System.out, PropertiesFile::new);
        propertiesFileCli.processInput();
    }

    private final String[] args;
    private final PrintStream printStream;
    private final Function<Path, PropertiesFile> propertiesFileCreator;

    public PropertiesFileCli(String[] args, PrintStream printStream, Function<Path, PropertiesFile> propertiesFileCreator) {
        this.args = args;
        this.printStream = printStream;
        this.propertiesFileCreator = propertiesFileCreator;
    }

    public void processInput() {
        if (args.length < 3 || (args.length - 1) % 2 != 0) {
            printUsage(WRONG_ARGS);
            return;
        }

        String propertiesFileLocation = args[0];
        Path propertiesFilePath = Paths.get(propertiesFileLocation);
        if (!propertiesFilePath.toFile().isFile()) {
            printUsage(NOT_A_FILE);
            return;
        }

        List<KeyValue> keyValues = new LinkedList<>();
        for (int keyIndex = 1; keyIndex < args.length; keyIndex += 2) {
            keyValues.add(new KeyValue(args[keyIndex], args[keyIndex + 1]));
        }

        PropertiesFile propertiesFile = propertiesFileCreator.apply(propertiesFilePath);
        propertiesFile.updateValues(keyValues);
        propertiesFile.store();
    }

    private void printUsage(String errorMessage) {
        printStream.println(errorMessage);
        try (InputStream inputStream = PropertiesFileCli.class.getResourceAsStream("/usage.txt")) {
            printStream.println(new String(inputStream.readAllBytes(), StandardCharsets.UTF_8));
        } catch (IOException e) {
            printStream.println("Couldn't load usage: " + e.getMessage());
        }
    }

}

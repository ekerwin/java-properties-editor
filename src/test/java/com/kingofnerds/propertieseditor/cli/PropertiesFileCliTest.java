package com.kingofnerds.propertieseditor.cli;

import com.kingofnerds.propertieseditor.PropertiesFile;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mockito;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PropertiesFileCliTest {
    @Test
    public void testBadFileProvided() {
        PropertiesFile propertiesFile = Mockito.mock(PropertiesFile.class);
        Function<Path, PropertiesFile> propertiesFileCreator = path -> propertiesFile;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(outputStream, true, StandardCharsets.UTF_8);

        PropertiesFileCli propertiesFileCli = new PropertiesFileCli(new String[]{"not a file", "a.matched.key", "the.matched.value"}, output, propertiesFileCreator);
        propertiesFileCli.processInput();

        String outputContent = outputStream.toString(StandardCharsets.UTF_8);
        assertTrue(outputContent.contains(PropertiesFileCli.NOT_A_FILE));

        Mockito.verifyNoInteractions(propertiesFile);
    }

    @Test
    public void testUnmatchedKeyValues(@TempDir Path tempDir) throws IOException {
        PropertiesFile propertiesFile = Mockito.mock(PropertiesFile.class);
        Function<Path, PropertiesFile> propertiesFileCreator = path -> propertiesFile;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(outputStream, true, StandardCharsets.UTF_8);

        Path testPath = Files.createFile(tempDir.resolve("test.properties"));
        PropertiesFileCli propertiesFileCli = new PropertiesFileCli(new String[]{testPath.toString(), "just.a.single.key"}, output, propertiesFileCreator);
        propertiesFileCli.processInput();

        String outputContent = outputStream.toString(StandardCharsets.UTF_8);
        assertTrue(outputContent.contains(PropertiesFileCli.WRONG_ARGS));

        Mockito.verifyNoInteractions(propertiesFile);
    }

    @Test
    public void testValidInput(@TempDir Path tempDir) throws IOException {
        PropertiesFile propertiesFile = Mockito.mock(PropertiesFile.class);
        Function<Path, PropertiesFile> propertiesFileCreator = path -> propertiesFile;

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(outputStream, true, StandardCharsets.UTF_8);

        Path testPath = Files.createFile(tempDir.resolve("test.properties"));
        PropertiesFileCli propertiesFileCli = new PropertiesFileCli(new String[]{testPath.toString(), "a.matched.key", "the.matched.value"}, output, propertiesFileCreator);
        propertiesFileCli.processInput();

        String outputContent = outputStream.toString(StandardCharsets.UTF_8);
        assertTrue(StringUtils.isBlank(outputContent));

        Mockito.verify(propertiesFile).updateValues(Mockito.argThat((list) -> list.size() == 1 && list.get(0).getKey().equals("a.matched.key") && list.get(0).getValue().equals("the.matched.value")));
        Mockito.verify(propertiesFile).store();
    }

}

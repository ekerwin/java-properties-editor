package com.kingofnerds.propertieseditor.model;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PropertyLinesTest {
    @Test
    public void testUpdateValues() {
        List<String> lines = List.of(
                "cat.name=Fluffy",
                "dog.name Doofus",
                "fish.name:Bubbles"
        );

        List<String> expected = List.of(
                "cat.name=Cuddles",
                "dog.name Slobberface",
                "fish.name:Gill"
        );

        PropertyLines propertyLines = new PropertyLines(lines);
        propertyLines.updateValue(new KeyValue("cat.name", "Cuddles"));
        propertyLines.updateValue(new KeyValue("dog.name", "Slobberface"));
        propertyLines.updateValue(new KeyValue("fish.name", "Gill"));

        assertEquals(expected, propertyLines.getLines());
    }

}

package com.kingofnerds.propertieseditor.model;

import com.kingofnerds.propertieseditor.model.PropertyLine;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PropertyLineTest {
    private PropertyLine propertyLine;

    @Test
    public void testCommentLine() {
        String content = "   #just a comment";
        propertyLine = new PropertyLine(0, content);
        assertIsComment(content);
    }

    @Test
    public void testBlankAndWhitespaceLine() {
        propertyLine = new PropertyLine(0, "  \n  ");
        assertIsBlank();
    }

    @Test
    public void testStandardKeyValueLine() {
        propertyLine = new PropertyLine(0, "key=value");
        assertIsKeyValue("key", "value");
    }

    @Test
    public void testKeyValueWithEscapedEquals() {
        propertyLine = new PropertyLine(0, "insanekey\\=stillkey\\\\\\=STILLKEY=madness");
        assertIsKeyValue("insanekey=stillkey\\=STILLKEY", "madness");
    }

    @Test
    public void testKeyValueWithEscapedColon() {
        propertyLine = new PropertyLine(0, "insanekey\\:stillkey\\\\\\:STILLKEY:madness");
        assertIsKeyValue("insanekey:stillkey\\:STILLKEY", "madness");
    }

    private void assertIsBlank() {
        assertTrue(propertyLine.isBlank());
        assertFalse(propertyLine.isComment());
        assertFalse(propertyLine.isKeyValue());
        assertFalse(propertyLine.isNonsense());

        assertTrue(StringUtils.isBlank(propertyLine.getContent()));
    }

    private void assertIsComment(String content) {
        assertFalse(propertyLine.isBlank());
        assertTrue(propertyLine.isComment());
        assertFalse(propertyLine.isKeyValue());
        assertFalse(propertyLine.isNonsense());

        assertEquals(content, propertyLine.getContent());
    }

    private void assertIsKeyValue(String key, String value) {
        assertFalse(propertyLine.isBlank());
        assertFalse(propertyLine.isComment());
        assertTrue(propertyLine.isKeyValue());
        assertFalse(propertyLine.isNonsense());

        assertEquals(key, propertyLine.getKey().get());
        assertEquals(value, propertyLine.getValue().get());
    }

    private void assertIsNonsense() {
        assertFalse(propertyLine.isBlank());
        assertFalse(propertyLine.isComment());
        assertFalse(propertyLine.isKeyValue());
        assertTrue(propertyLine.isNonsense());
    }

}

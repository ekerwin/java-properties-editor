package com.kingofnerds.propertieseditor;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PropertiesTest {
    @Test
    public void testPropertiesWithEqualsKey() throws IOException {
        String key = "value\\=withequals";
        String keyText = "value=withequals";
        String value = "whatever";
        Reader reader = new StringReader(String.format("%s=%s", key, value));

        Properties properties = new Properties();
        properties.load(reader);

        assertTrue(properties.stringPropertyNames().contains(keyText));
        assertEquals(value, properties.getProperty(keyText));
    }

}

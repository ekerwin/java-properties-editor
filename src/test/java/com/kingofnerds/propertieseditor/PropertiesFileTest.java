package com.kingofnerds.propertieseditor;

import com.kingofnerds.propertieseditor.model.KeyValue;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PropertiesFileTest {
    @TempDir
    Path temporaryDirectory;

    @Test
    public void testFullFileEdit() throws IOException, URISyntaxException {
        InputStream samplePropertiesSource = getClass().getResourceAsStream("/sample.properties");
        Path sampleCopy = Files.createFile(temporaryDirectory.resolve("sample_copy.properties"));
        FileUtils.copyInputStreamToFile(samplePropertiesSource, sampleCopy.toFile());

        List<KeyValue> keyValues = List.of(
                new KeyValue("monkey.name", "Aiai"),
                new KeyValue("gorilla.name", "Kong"),
                new KeyValue("whale.name", "Jonah")
        );

        PropertiesFile propertiesFile = new PropertiesFile(sampleCopy.toFile());

        propertiesFile.updateValues(keyValues);
        propertiesFile.updateValue("lion.name", "Leo");

        propertiesFile.store();

        File expected = new File(getClass().getClassLoader().getResource("sample_expected.properties").toURI());
        File actual = sampleCopy.toFile();
        assertTrue(FileUtils.contentEquals(expected, actual));
    }

}
